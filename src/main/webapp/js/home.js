/**
 * 
 */

app.controller('homeController', function($scope, $http) {
	$scope.getSearch = () => {
		$scope.userList = {};
		console.log("getting users from search user: " + $scope.searchUser);
		$http.post(domain + "searchUsers.app", $scope.searchUser)
		.then((successResponse) => {
			$scope.userList = successResponse.data;
		})
	}
	
	//this will throw a bad request error because getPosts.app needs a last post sent as a parameter
	$scope.getAllPosts = () => {
		$scope.allPostList = {};
		console.log("Selecting all posts");
		$http.post(domain + "getPosts.app", blankPost)
		.then((successResponse) => {
			console.log("successResponse before");
			$scope.allPostList = successResponse.data;
		})
	}
	
	$scope.likePost = (inPost, inUser) => {
		$scope.postList = {};
		console.log("select a post works " + inUser.id + " Post ID: " + inPost.id);
		//below needs to be changed
		$http.post(domain + "toggleLike.app", inPost, inUser)
		.then((successResponse) => {
			console.log("successResponse before");
			$scope.likePost = successResponse.data;
		})
	}
	
	$scope.getSelectedUserPosts = (inUser) => {
		$scope.postList = {};
		console.log("Selected User Post after Works. user: " + inUser.id);
		$http.post(domain + "getUserPost.app", inUser)
		.then((successResponse) => {
			console.log("successResponse before");
			$scope.postList = successResponse.data;
		})
	}
	
	$scope.submitPost = () => {
		$scope.userPost = {};
		console.log("Inserting user post");
		//put user post
		//insert the image, the post, the user creator
		console.log("Displaying user post: " + $scope.userPost);
		$http.post(domain + "insertPost.app", $scope.userPost)
		.then((successResponse) => {
			$scope.addedPost = successResponse.data;
		})
	}
	
	$scope.editInfo = () => {
		//im not sure what to do here
		//i want to change the values to inputs then save what the user has changed
		$scope.editUser = {}
		console.log("Editing user info");
		console.log("first name = "+ $scope.userFirstName.value);
		//needs to be persisted in the database and then persisted on the page
		
		//updateUserInformation.app
	}
	
	$scope.editPicture = () => {
		//updateUserProfilePicture.app
	}
	
});

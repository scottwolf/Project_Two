package com.revature.util;

import java.security.MessageDigest;

/**
 * Provides a way to easily hash data.
 * @author user1452273 (Stack Overflow)
 */
public class HashUtil {
	
	/**
	 * Hashes the given string.
	 * @param string for hashing
	 * @return the hashed string
	 */
	public static String hash(String string) {
		try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(string.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }

	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
}

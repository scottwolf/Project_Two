package com.revature.model;

public class LikedPostWrapper {
	
	private int postId;
	private String username;
	public LikedPostWrapper(int postId, String username){
		this.postId = postId;
		this.username = username;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}

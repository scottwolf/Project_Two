package com.revature.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.revature.ajax.AjaxMessage;
import com.revature.model.Post;
import com.revature.model.User;
import com.revature.service.PostService;
import com.revature.service.UserService;
import com.revature.util.AjaxMessageUtil;
import com.revature.util.FinalUtil;

@Controller
/**
 * Responds to request mapper to handle HTTP requests from frontend
 * Only concerned with posts 
 * @author Scott Wolf
 *
 */
public class PostController {
	
	public PostController() {}
	
	@Autowired
	private PostService postService;
	
	@Autowired
	private UserService userService;
	
	/**
	 * Passes to service
	 * @param lastPost -  last post presented to user
	 * @return - list of next ten posts
	 */
	@RequestMapping("/getPosts.app")
	public @ResponseBody List<Post> getPosts(@RequestBody Post lastPost){
		return postService.retrieveAllPosts(lastPost);
	}
	
	/**
	 * Used to get the initial page displayed to the user
	 * @return - list of first ten posts, sorted by timestamp (creationDate)
	 */
	@RequestMapping("/getInitialPosts.app")
	public @ResponseBody List<Post> getInitialPosts(){
		return postService.retrieveAllInitialPosts();
	}
	
	/**
	 * Returns the posts created by a individual user
	 * @param user - user to look up posts
	 * @return - list of that user's posts
	 */
	@RequestMapping("/getUserPost.app")
	public @ResponseBody List<Post> getPostUsername(@RequestBody User user){
		return postService.retrievePostsByUsername(user);
	}
	
	/**
	 * Hands off creation of new post to service
	 * send in null value for date, service adds in timestamp
	 * @param post - Post object with content, image (opt), user who created
	 * 
	 * @return - list of most recent posts, with new post on top
	 */
	@RequestMapping("/insertPost.app")
	public @ResponseBody List<Post> insertPost(@RequestBody Post post){
		User postCreator = post.getPostCreator();
		postCreator = userService.retrieveUser(postCreator);
		post.setPostCreator(postCreator);
		postService.submitPost(post);
		return postService.retrieveAllInitialPosts();
	}
	
	/**
	 * Takes user who liked post, and post that was liked
	 * @param post - post w/ id 
	 * @param user - user who liked posted
	 * @return - returns ajax message with success code
	 */
	@RequestMapping("/like.app")
	public @ResponseBody AjaxMessage toggleLike(@RequestBody String username){
		String[] two = username.split(",");
		int postId = Integer.parseInt(two[0]);
		Post likedPost = new Post(postId);
		User likedUser = new User(two[1]);
		likedUser = userService.retrieveUser(likedUser);
		likedPost = postService.getPostById(likedPost);
		System.out.println("IN CONTROLLER AFTER PULLING POST FROM DB, POST  = " + likedPost.toString());
		System.out.println("IN CONTROLLER AFTER PULLING POST FROM DB, LIKED LIST = " + likedPost.getUserWhoLiked());
		System.out.println("IN CONTROLLER AFTER PULLING USER FROM DB, USER = " + likedUser.toString());
		System.out.println("IN CONTROLLER AFTER PULLING USER FROM DB, USER LIKED LIST = " + likedUser.getLikesPostId());
		postService.toggleLike(likedPost, likedUser);
		return AjaxMessageUtil.getAjaxMessage(FinalUtil.LIKE_POST_CODE);
	}
}

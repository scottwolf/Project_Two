package com.revature.service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.model.Post;
import com.revature.model.User;
import com.revature.repository.PostRepository;

@Service("postService")
public class PostServiceSpring implements PostService {

	@Autowired
	private PostRepository postRepository;

	public PostServiceSpring(){};

	@Transactional
	public List<Post> retrieveAllPosts(Post lastPost) {
		return postRepository.selectAll(lastPost);
	}
	
	@Transactional
	public List<Post> retrievePostsByUsername(User user) {
		return postRepository.selectByUsername(user);
	}

	@Transactional
	public void submitPost(Post newPost) {
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
		newPost.setCreatedTime(currentTimestamp);
		postRepository.insert(newPost);
	}

	@Transactional
	public void deletePost(Post post) {
		postRepository.delete(post);
	}
	
	@Transactional
	public void toggleLike(Post post, User user){
		if(post.getUserWhoLiked().contains(user)){
			System.out.println("TOGGLE OFF + " + post.getUserWhoLiked());
			System.out.println(post.getUserWhoLiked().isEmpty());
			postRepository.toggleLikeOff(post, user);
		} else{
			System.out.println("TOGGLE ON + " + post.getUserWhoLiked());
			System.out.println(post.getUserWhoLiked().isEmpty());
			postRepository.toggleLikeOn(post, user);
		}
	}
	@Override
	public List<Post> retrieveAllInitialPosts() {
		return postRepository.selectAllInitial();
	}
	
	@Override
	public Post getPostById(Post post){
		return postRepository.selectPost(post);
	}
}

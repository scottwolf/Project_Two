package com.revature.service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.model.User;
import com.revature.model.UserMisc;
import com.revature.repository.UserRepository;
import com.revature.util.FinalUtil;
import com.revature.util.HashUtil;
import com.revature.util.ResetEmailThread;

@Service("userService")
public class UserServiceSpring implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public UserServiceSpring() {};
	@Override
	public boolean isUniqueUsername(User user) {
		return !user.getUsername().equals(userRepository.selectByUsername(user).getUsername());
	}
	@Override
	public User retrieveUser(User user) {
		return userRepository.selectByUsername(user);
	}
	@Override
	public User registerUser(User user) {
		User newUser = new User(user.getId(), user.getUsername(), user.getPassword(), user.getEmail(), user.getFirstName(),
				user.getLastName(), user.getProfilePicture(), user.getLikesPostId());
		
		newUser.setPassword(HashUtil.hash(newUser.getPassword()));
		if (newUser.getProfilePicture() == null || newUser.getProfilePicture().equals("")) {
			newUser.setProfilePicture(FinalUtil.DEFAULT_PROFILE_PICTURE);
		}
		userRepository.insert(newUser);
		return userRepository.selectByUsername(newUser);
	}
	@Override
	public void unregisterUser(User user) {
		userRepository.deleteByUsername(user);
	}
	@Override
	public User resetUser(User user) {
		unregisterUser(user);
		return registerUser(user);
	}
	@Override
	public User updateUserInformation(User userToUpdate) {
		User updatedUser = userRepository.select(userToUpdate);
		
		// Only update information-specific columns
		updatedUser.setUsername(userToUpdate.getUsername());
		updatedUser.setEmail(userToUpdate.getEmail());
		updatedUser.setFirstName(userToUpdate.getFirstName());
		updatedUser.setLastName(userToUpdate.getLastName());
		
		userRepository.update(updatedUser);
		
		return updatedUser;
	}
	@Override
	public User updateUserPassword(User userToUpdate) {
		User updatedUser = userRepository.select(userToUpdate);
		
		// Only update password
		updatedUser.setPassword(HashUtil.hash(userToUpdate.getPassword()));
		
		userRepository.update(updatedUser);
		
		return updatedUser;
	}
	@Override
	public User updateUserProfilePicture(User userToUpdate) {
		User updatedUser = userRepository.select(userToUpdate);
		
		// Only update image
		updatedUser.setProfilePicture(userToUpdate.getProfilePicture());
		
		userRepository.update(updatedUser);
		
		return updatedUser;
	}
	@Override
	public User login(User user) {
		User loggedUser = userRepository.selectByUsername(user);
		
		if(loggedUser.getPassword().equals(HashUtil.hash(user.getPassword()))) {
			return loggedUser;
		}
		
		return new User();
	}
	@Override
	public List<User> findUsers(UserMisc userMisc) {
		User user = new User();
		String[] names = userMisc.getName().split("[ ]");
		
		if (names.length == 2) {
			user.setFirstName(names[0]);
			user.setLastName(names[1]);
			return userRepository.selectAllByFullNameBothWays(user);
		} else if (names.length == 1) {
			user.setFirstName(names[0]);
			user.setLastName(names[0]);
			return userRepository.selectAllByFirstOrLastName(user);
		}
		
		return new ArrayList<User>();
	}
	@Override
	public User resetUserPassword(User user) {
		User dbUser = userRepository.selectByUsername(user);
		
		String newPassword = generatePassword(FinalUtil.GENERATED_PASSWORD_LENGTH);
		dbUser.setPassword(newPassword);
		updateUserPassword(dbUser);
		
		return dbUser;
	}
	@Override
	public void emailResetPassword(User user) {
		new ResetEmailThread(user).start();
	}
	
	private String generatePassword(int length) {
		StringBuffer password = new StringBuffer("");
		SecureRandom rnd = new SecureRandom();
		
		for (int i = 0; i < length; i++) {
			password.append(FinalUtil.ALPHANUMERIC.charAt(rnd.nextInt(FinalUtil.ALPHANUMERIC.length())));
		}
		
		return password.toString();
	}
	
}

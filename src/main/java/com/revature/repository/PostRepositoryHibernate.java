package com.revature.repository;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.revature.model.Post;
import com.revature.model.User;
import com.revature.util.FinalUtil;

@Repository("postRepository")
@Transactional
public class PostRepositoryHibernate implements PostRepository {

	@Autowired
	SessionFactory sessionFactory;

	public PostRepositoryHibernate(){};

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> selectByUsername(User user) {
		try{
			return (ArrayList<Post>) sessionFactory.getCurrentSession().createCriteria(Post.class)
					.add(Restrictions.like("postCreator",user))
					.addOrder(Order.desc("createdTime"))
					.setMaxResults(FinalUtil.MAX_POST_LIST_SIZE)
					.list();
		}catch(IndexOutOfBoundsException e){
			return new ArrayList<Post>();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> selectAll(Post lastPost) {
		return  sessionFactory.getCurrentSession().createCriteria(Post.class)
				.addOrder(Order.desc("createdTime"))
				.setFirstResult(lastPost.getId())
				.setMaxResults(FinalUtil.MAX_POST_LIST_SIZE)
				.list();
	}
	@Override
	public void insert(Post newPost) {
		//newPost.setPostCreator(postCreator);
		sessionFactory.getCurrentSession().save(newPost);
	}
	@Override
	public void delete(Post postToDelete) {
		System.out.println("Post to delete id = " + postToDelete.getId());
		try{
			Session session = sessionFactory.getCurrentSession();
			Post deletedPost = (Post) session.createCriteria(Post.class)
					.add(Restrictions.like("id", postToDelete.getId()))
					.list()
					.get(0);
			session.delete(deletedPost);
		} catch(IndexOutOfBoundsException e) {		
			System.out.println("Dao failed to delete");
		}
	}
	@Override
	public void toggleLikeOff(Post post, User user) {
		post.getUserWhoLiked().remove(user);
		System.out.println("In toggle like on " + post.getUserWhoLiked());
		post.setLikeCount(post.getLikeCount()-1);
		sessionFactory.getCurrentSession().update(post);
	}
	@Override
	public void toggleLikeOn(Post post, User user) {
		post.setLikeCount(post.getLikeCount()+1);
		/*User nullcopy = user;
		nullcopy.nullObjectSet();
		Post nullpost = post;
		nullpost.nullObjectSet();
		user.setLikesPost(nullpost);
		post.setUserWhoLiked(nullcopy);
		//sessionFactory.getCurrentSession().evict(user);*/
		sessionFactory.getCurrentSession().update(post);
		//sessionFactory.getCurrentSession().update(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> selectAllInitial() {
		return  sessionFactory.getCurrentSession().createCriteria(Post.class)
				.addOrder(Order.desc("createdTime"))
				.setMaxResults(FinalUtil.MAX_POST_LIST_SIZE)
					.list();
	}

	public Post selectPost(Post post){
		try{
			return (Post) sessionFactory.getCurrentSession().createCriteria(Post.class)
					.add(Restrictions.like("id", post.getId()))
					.list()
					.get(0);
		} catch(IndexOutOfBoundsException e) {		
			return new Post();
		}
	}
}

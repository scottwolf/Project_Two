package com.revature.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.revature.model.User;

@Repository("userRepository")
@Transactional
public class UserRepositoryHibernate implements UserRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private UserRepositoryHibernate() {}
	
	public User select(User user) {
		try{
			return (User) sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.like("id", user.getId()))
				.list()
				.get(0);
		} catch(IndexOutOfBoundsException e) {		
			return new User();
		}
	}
	@Override
	public User selectByUsername(User user) {
		try{
			return (User) sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.like("username", user.getUsername()))
				.list()
				.get(0);
		} catch(IndexOutOfBoundsException e) {		
			return new User();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAllByFirstName(User user) {
		return sessionFactory.getCurrentSession().createCriteria(User.class)
			.add(Restrictions.like("firstName", user.getFirstName() + "%"))
			.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAllByLastName(User user) {
		return sessionFactory.getCurrentSession().createCriteria(User.class)
			.add(Restrictions.like("lastName", user.getLastName() + "%"))
			.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAllByFullName(User user) {
		return sessionFactory.getCurrentSession().createCriteria(User.class)
			.add(Restrictions.and(Restrictions.like("firstName", user.getFirstName() + "%"),
					Restrictions.like("lastName", user.getLastName() + "%")))
			.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAllByFullNameReverse(User user) {
		return sessionFactory.getCurrentSession().createCriteria(User.class)
			.add(Restrictions.and(Restrictions.like("firstName", user.getLastName() + "%"),
					Restrictions.like("lastName", user.getFirstName() + "%")))
			.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAllByFullNameBothWays(User user) {
			Criterion full, fullReverse;
			full = Restrictions.and(Restrictions.like("firstName", user.getFirstName() + "%"),
					Restrictions.like("lastName", user.getLastName() + "%"));
			fullReverse = Restrictions.and(Restrictions.like("firstName", user.getLastName() + "%"),
					Restrictions.like("lastName", user.getFirstName() + "%"));
			
			return sessionFactory.getCurrentSession().createCriteria(User.class)
			.add(Restrictions.or(full, fullReverse))
			.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectAllByFirstOrLastName(User user) {
		return sessionFactory.getCurrentSession().createCriteria(User.class)
			.add(Restrictions.or(Restrictions.like("firstName", user.getFirstName() + "%"),
					Restrictions.like("lastName", user.getLastName() + "%")))
			.list();
	}
	@Override
	public void insert(User newUser) {
		sessionFactory.getCurrentSession().save(newUser);
	}
	@Override
	public void update(User userToUpdate) {
		sessionFactory.getCurrentSession().update(userToUpdate);
	}
	@Override
	public void deleteByUsername(User userToDelete) {
		try{
			Session session = sessionFactory.getCurrentSession();
			User u = (User) session.createCriteria(User.class)
			.add(Restrictions.like("username", userToDelete.getUsername()))
			.list()
			.get(0);
			session.delete(u);
		} catch(IndexOutOfBoundsException e) {		
			
		}
	}

}

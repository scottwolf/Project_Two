NOTE: All request parameters should be passed in a single object.
NOTE: It is usually a good idea to provide all request parameters, or else things may not work as expected.

/getUser.app
Request: username
Response: single User object

/searchUsers.app
Request: name
Response: list of User objects

/registerUser.app
Request: username, password, email, firstName, lastName
Response: single User object

/login.app
Request: username, password
Response: single User object

/updateUserInformation.app
Request: username, email, firstName, lastName
Response: single User object

/updateUserPassword.app
Request: password
Response: single User object

/updateUserProfilePicture.app
Request: profilePicture (full URL)
Response: single User object

/checkUniqueUsername.app
Requst: username
Response: single object with a code (number) and a message (string). A code of 600 means unique and a code of 601 means not unique. The message says "USERNAME AVAILABLE" or "USERNAME TAKEN" depending on the code.

/logout.app